/* Author:http://www.rainatspace.com

*/

function initializeScript(){
	//jQuery('#preloader').fadeOut(2000);
		jQuery("[icon-data]").each(function(){
		var getAttr =  jQuery(this).attr('icon-data');
		jQuery(this).addClass(getAttr).removeAttr('icon-data');
	});

	// Navigation  Bounce Menu Effect================================================================*/
	jQuery('.nav li ul').removeClass('hidden');
		jQuery('.nav li').hover(function() {
			jQuery('ul', this).filter(':not(:animated)').slideDown(50, 'easeInExpo');
	     }, function() {
		jQuery('ul', this).slideUp(10, 'easeInExpo');
	});

	// NAVIGATION RESPOSNIVE HANDLER
	jQuery(".nav >  ul").clone(false).appendTo(".nav-rwd-sidebar");
	jQuery(window).on('load', function(){
		jQuery('.nav-rwd-sidebar').find('ul').removeClass();
	});
	jQuery(".btn-rwd-sidebar, .btn-hide").click( function() {
		jQuery(".nav-rwd-sidebar").toggleClass("sidebar-active");
		jQuery(".wrapper-inner").toggleClass("wrapper-active");
	});


	//MAINSLIDE
	jQuery('.slideMain-holder').bxSlider({
	  	adaptiveHeight: true,
	  	pager: false,
	  	auto: true,
	  	speed: 800,
	  	pause: 6000
	});
	jQuery('.impaSlide').bxSlider({
	  	adaptiveHeight: true,
	  	mode:'fade',
	  	auto: true,
	  	speed: 1000,
	  	controls: false
	});

	jQuery('.teamSlider').bxSlider({
 		 pagerCustom: '#teamPager'
	});

	//FILTER
	jQuery('#filNav li a').click(function() {
		var ourClass = jQuery(this).attr('class');
		jQuery('#filNav li').removeClass('active');
		jQuery(this).parent().addClass('active');
		
		if(ourClass == 'all') {
			jQuery('.filterHolder').children('li').show();	
		}
		else {
			jQuery('.filterHolder').children('li:not(.' + ourClass + ')').hide();
			jQuery('.filterHolder').children('li.' + ourClass).show();
		}
		return false;
	});

	//EQUAL HEIGHT
    var tallest = 0;
	jQuery(window).on("load", function(){
		if (jQuery(window).width() >  767 ) {
			jQuery(".eqHeight").each(function() {
				var thisHeight = $(this).innerHeight();
				if(thisHeight > tallest) {
					tallest = thisHeight;
				}
			});
			jQuery(".eqHeight").innerHeight(tallest);
		}
	});

	jQuery('#teamPager .modalTogl').on('click', function(){
		jQuery('.modal-box').toggleClass('show', function(){
				jQuery('html,body').stop().animate({
						scrollTop: jQuery(jQuery('#teamPager')).offset().top + 50
				}, 800);
		});

	});

	jQuery('.modalClose').on('click', function(){
		jQuery(this).closest('.modal-box').removeClass('show');
	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();

    jQuery(".fancybox").fancybox({
    	beforeShow: function(){
   			this.title = jQuery(this.element).next('.newTitle').html();
  		},
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
/* END ------------------------------------------------------- */